<?php defined( '_JEXEC' ) or die; 

include_once JPATH_THEMES.'/'.$this->template.'/logic.php';

?><!doctype html>

<html lang="<?php echo $this->language; ?>">

<head>
	<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
  <jdoc:include type="head" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  
</head>

<?php $config = JFactory::getConfig(); ?>

<?php if ($this->countModules( 'sidebar1' ) or $this->countModules( 'sidebar2' ) or $this->countModules( 'sidebar3' )) 
{
	$sidebarActiveStatus = "sidebar-active";
}
else 
{
	$sidebarActiveStatus = "sidebar-inactive";	
}
?>

<?php
$app = JFactory::getApplication();
$menu = $app->getMenu();
if ($menu->getActive() == $menu->getDefault()) {
	$frontpageStatus = "is-frontpage";
}
else 
{
	$frontpageStatus = "not-frontpage";
}
?>

  
<body class="<?php echo (($menu->getActive() == $menu->getDefault()) ? ('front') : ('site')).' '.$active->alias.' '.$pageclass; ?> <?php echo $sidebarActiveStatus . ' ' . $frontpageStatus ?>">
	<header>
		<div class="header-inner">
			<div class="logo">
				<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/aal-logo-white.png"/>
			</div>
			<div class="nav-container wrapper">
				<jdoc:include type="modules" name="navigation" />
			</div>

		</div>
		<?php
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		if ($menu->getActive() == $menu->getDefault()) { ?>
			<div class="image-slider">
				<jdoc:include type="modules" name="imageSlider" />
			</div>
		<?php }
		?>
	</header>

  <div class="wrapper">
	  
	  <div class="main-content">
		  <jdoc:include type="component" />

	  </div>
	  <?php if ($this->countModules( 'sidebar1' ) or $this->countModules( 'sidebar2' ) or $this->countModules( 'sidebar3' )) : ?>
			  <div class="sidebar">
			  	<jdoc:include type="modules" name="sidebar1" style="xhtml" />
			  	<jdoc:include type="modules" name="sidebar2" style="xhtml" />
			  	<jdoc:include type="modules" name="sidebar3" style="xhtml" />
			  </div>
		<?php endif; ?>
  </div>
    <?php if ($this->countModules( 'highlight1' ) or $this->countModules( 'highlight2' ) or $this->countModules( 'highlight3' )) : ?>
  	  <div class="secondary-content">
  	  	<div class="wrapper">
	  	  	<jdoc:include type="modules" name="highlight1" style="xhtml" />
	  	  	<jdoc:include type="modules" name="highlight2" style="xhtml" />
	  	  	<jdoc:include type="modules" name="highlight3" style="xhtml" />
  	  	</div>
  	  </div>
    <?php endif; ?>
  <footer class="site-footer">
	  <div class="wrapper">
		  <jdoc:include type="modules" name="footer" style="xhtml" />
	  	<div class="copyright-info">&copy; <?php echo date('Y'); ?> <?php echo $config->get( 'sitename' ); ?></div>
  	</div>
  </footer>
  <jdoc:include type="modules" name="debug" />
</body>

</html>
